jQuery(document).ready(function () {

	var current_year=new Date().getFullYear();
	$('#year').val(current_year)

	function error(text) {
		var error = $('.error_label');
		error.prepend('<div class="label error">'+text+' <span class="close">×</span></div>');
	}
	function isAlphaOrParen(str) {
		return /^[a-zA-Z() ]+$/.test(str);
	}
	function objectLength(obj) {
		var result = 0;
		for(var prop in obj) {
			if (obj.hasOwnProperty(prop)) {
			    // or Object.prototype.hasOwnProperty.call(obj, prop)
			    result++;
			}
		}
		return result;
	}
	// return month full name
	function monthFullName(month) {
		var monthName = new Array('none', 'January', 'February', 'March', 'April', 'May', 'June',
			'July', 'August', 'September', 'October', 'November', 'December');
		return monthName[month];
	}




	$('body').on('click', 'span.close',function() {
		$(this).parent().remove();
	})




	$('.btn_submit').click(function() {
		var city = $('#city').val();
		var country = $('#country').val();
		var year = parseInt($('#year').val());
		var method = $('#method').val();
		var school = $('#school').val();
		$('.info').html('Please choose your location to view prayer times of your location.')
		$('.table #month').html('');

		// clearing errors
		var error_container = $('.error_label');
		error_container.html('');

		$('.table').removeClass('hide_note');
		$('.btn_submit').addClass('disabled');

		// clearing adhan table
		var table_body = $('.table tbody');
		table_body.remove();



		// Checking CITY
		if (city != "") {
			if(!isAlphaOrParen(city)){
				error("<b>City name</b> is invalid.");
				$('.btn_submit').removeClass('disabled');
				return false;
			}
		}else{
			error("<b>City name</b> is required.");
			$('.btn_submit').removeClass('disabled');
			return false;
		}



		// Checking COUNTRY
		// if (country == "none" || country == "") {
		// 	error("<b>Country name</b> is required.");
		// 	$('.btn_submit').removeClass('disabled');
		// 	return false;
		// }




		// Checking METHOD
		if (method == "none" || method == "") {
			error("<b>Calculation Method</b> name is required.");
			$('.btn_submit').removeClass('disabled');
			return false;
		}




		// Checking SCHOOL
		if (school == "none" || school == "") {
			error("<b>Juristic Method</b> name is required.");
			$('.btn_submit').removeClass('disabled');
			return false;
		}



		// Checking YEAR
		var year_text = /^[0-9]+$/;
		if ($('#year').val() != "") {
			var current_year=new Date().getFullYear();

			if ((!year_text.test(year))) {
				error("<b>Year</b> is invalid. Please check and try again.");
				$('.btn_submit').removeClass('disabled');
				return false;
			}

			if ($('#year').val().length != 4) {
				error("<b>Year</b> is not proper (Ex. 1994). Please check and try again. ");
				$('.btn_submit').removeClass('disabled');
				return false;
			}

		}else{
			error("<b>Year</b> is required.");
			$('.btn_submit').removeClass('disabled');
			return false;
		}


		$('.info').html('<b>Loading Adhan table...</b>')
		$.ajax({
			url: 'http://api.aladhan.com/v1/calendarByCity?city='+city+'&country='+country+'&method='+method+'&school='+school+'&annual=true',
			type: 'GET',
			success: function(data){
				$('.table').addClass('hide_note');
				$('.btn_submit').removeClass('disabled');

				var whole_year_data = data['data'];
				var first_month = "";

				for (outerLoopCounter = 1; outerLoopCounter < objectLength(whole_year_data) + 1; outerLoopCounter++) {
					var month_data = whole_year_data[outerLoopCounter];
					var current_month = monthFullName(outerLoopCounter);

					$('.table table').append('<tbody class="'+ current_month +'"></tbody>');
					var current_month_table = $('.table table .' + current_month);

					if (outerLoopCounter == 1) {
						first_month = current_month;
						$('.table #month').append('<option selected="selected" value="'+ current_month +'">'+ current_month +'</option>');
					}else{
						$('.table #month').append('<option value="'+ current_month +'">'+ current_month +'</option>');
					}

					for (i = 0; i < month_data.length; i++) {
						var namaz_timings = month_data[i]['timings'];
						var current_day = month_data[i]['date']['gregorian']['day'];

						var Fajr = namaz_timings['Fajr'];
						var Sunrise = namaz_timings['Sunrise'];
						var Dhuhr = namaz_timings['Dhuhr'];
						var Asr = namaz_timings['Asr'];
						var Maghrib = namaz_timings['Maghrib'];
						var Isha = namaz_timings['Isha'];
						var Imsak = namaz_timings['Imsak'];
						var Midnight = namaz_timings['Midnight'];
						var Sunset = namaz_timings['Sunset'];

						current_month_table.append('<tr> <td>'+ current_day +'</td> <td>'+ Fajr +'</td> <td>'+ Sunrise +'</td> <td>'+ Dhuhr +'</td> <td>'+ Asr +'</td> <td>'+ Maghrib +'</td> <td>'+ Isha +'</td> </tr>');
					}
				}
				$('.table tbody').hide();
				$('.table tbody.'+first_month).show();

			},
			error: function(data) {
				error("<b>" + data['responseJSON']['data'] + "</b>");
			}
		});


	});


$('body').on('change', '#month',function() {
	var current_selected_month = $(this).val();
	$('.table tbody').hide();
	$('.table tbody.'+current_selected_month).show();
});

});
